# Flask settings
flask_port = 7000

# endpoint for doctor details
GetODocStatus = "https://dev.odoc.life/odocapi/api/Patient/GetODOCStatus"

# GetODocStatus response parsing
kDoctorsInQueue = "DoctorsInQueue"
kDoctorID = "DoctorID"
kTitle = "Title"
kFirstName = "FirstName"
kLastName = "LastName"
kWaittime = "AverageWaitTime"

kQuote = "Quote"
kValue = "Value"

kSpeciality = "Speciality"

kSpecialityDescription = "SpecialityDescription"
kCityOfResidence = "CityOfResidence"
kCountryOfResidence = "CountryOfResidence"
kSLMCNumber = "SLMCNumber"
kStatement = "Statement"
kEducation = "Education"
kQualification = "Qualification"
kPracticeLocations = "PracticeLocations"
kProfilePicture = "PRO_PIC"
kThumbnailPicture = "thumbnail"
kHometown = "Hometown"
kBoardCertification = "BoardCertification"
kQualification = "Qualification"
kCanSinhala = "CanSinhala"
kCanEnglish = "CanEnglish"
kCanTamil = "CanTamil"
kLanguagesSpoken = "LanguagesSpoken"
kNumReviews = "TotalReview"
kRating = "Rating"
kSearchTag = "SearchTag"
kPrice = "Price"
kSimilarDoctorsTitleString = "SimilarDoctorsTitleString"

kTestimonials = "TestimonialList"
kTestimonialText = "Review"
kTestimonialProfilePic = "PRO_PIC"
kTestimonialName = "Name"
kTestimonialDate = "TestimonialDate"

kRowTitles = "rowTitles"
kThoughtsOnOdoc = "ThoughtsOnOdoc"
kSimilarDoctors = "SimilarDoctors"

kSpotText = "SpotText"
kAwards = "Awards"