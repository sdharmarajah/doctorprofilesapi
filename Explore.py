class Explore(object):
    def __init__(self, image_url, display_text, search_text):
        self.image_url = image_url
        self.display_text = display_text
        self.search_text = search_text

    def to_dict(self):
        return {
            "image_url": self.image_url,
            "display_text": self.display_text,
            "search_text": self.search_text
        }
