from pprint import pprint


def generate_homescreen(doctor_list, row_titles):
    if len(doctor_list) == 0:
        return "Doctor list was null. Please make a GET request to /refresh"

    # add these rows to the return dict
    homescreen = {}
    for row_number, (title, subtitle) in row_titles.items():
        row_content = {
            "title": title,
            "subtitle": subtitle,
            "doctors": list()
        }
        homescreen.update({row_number: row_content})

    # loop over doctors and add to the doctors key
    for doctor in doctor_list:
        for row_number, row_content in homescreen.items():
            if row_content['title'] == doctor.speciality:
                homescreen[row_number]["doctors"].append(doctor.to_dict(view_type="homescreen"))
                break

    # return homescreen as an ordered list
    homescreen_list = list()

    # append doctors on homescreen
    indices = list(homescreen.keys())
    indices.sort(key=int)
    for index in indices:
        homescreen_list.append(homescreen.get(index))

    return homescreen_list


def get_doctor_details(doctor_list, doctor_id, row_title):
    if len(doctor_list) == 0:
        return "Doctor list was null. Please make a GET request to /refresh"
    for doctor in doctor_list:
        if doctor.doctor_id == doctor_id:
            # generate the similar_docs list for this doctor

            # loop over other doctors and add if different doc with same speciality
            similar_docs_list = list()
            for other_doc in doctor_list:
                if doctor.doctor_id != other_doc.doctor_id and other_doc.speciality.lower() == row_title.lower():
                    similar_docs_list.append(other_doc.to_dict(view_type="homescreen"))

            doctor.similar_docs = similar_docs_list

            return doctor.to_dict(view_type="details")
