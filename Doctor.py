from settings import *
import random
from flask import jsonify
from pprint import pprint


class Doctor(object):
    def __init__(self, json):
        self.doctor_id = json.get(kDoctorID)
        self.title = json.get(kTitle, "Dr.")
        self.first_name = json.get(kFirstName, "Rohitha")
        self.last_name = json.get(kLastName, "Wickremasinghe")
        self.speciality = json.get(kSpeciality, "Pediatrician")
        self.wait_time = json.get(kWaittime, "12h")
        self.speciality_description = json.get(kSpecialityDescription, "Children’s Health")
        self.cityOfResidence = json.get(kCityOfResidence, "Colombo")
        self.countryOfResidence = json.get(kCountryOfResidence, "Sri Lanka")
        self.statement = json.get(kStatement, "Person is a kind and trusted Adolescent medicine specialist assisting patients in Colombo, Sri Lanka. Dr. Person is multilingual, speaking English, Sinhala. She is certified by PGIM Sri Lanka, The American Board of Medical Specialties (ABMS) and The American Board of Physician Specialties (ABPS). Dr. Person's clinical procedural interests include Abdominoplasty (Tummy Tuck ( Abdominoplasty)), Ablation Therapy for Arrhythmias, Ablation and Endometrial (Endometrial Ablation). She received her medical degree from University of Moratuwa, where she recieved training in Accident and emergency medicine, Addiction medicine and Adolescent medicine. Outside of work, Dr. Person enjoys playing sports. She is available to her patients at Agalawatta Base Hospital, Pimbura, Air Force Hospital, SLAF Katunayake, Katunayake, Colomb Naval Hospital, SLNS Parakrama, Colombo, Colombo Military Hospital and Colombo")
        self.education = json.get(kEducation, "University of Moratuwa")

        self.spot_text = json.get(kSpotText, "")
        self.awards = json.get(kAwards, "")

        if self.doctor_id == 21:
            self.spot_text = json.get(kSpotText, "There’s only few spots left. Join over 300 others in booking Dr. Janaka this week.")
            self.awards = json.get(kAwards, "DCH - RCPCH UK 2000")
        elif self.doctor_id == 39:
            self.spot_text = json.get(kSpotText, "There’s only few spots left. Join over 50 others in booking Dr. Punyajith this week.")
        elif self.doctor_id == 1:
            self.awards = json.get(kAwards, "MD/PhD - Harvard 1969")

        # TODO: use actual picture, not randomly generated one:
        # self.profile_pic = json.get(kProfilePicture, "https://dev.odoc.life/oDocApi/Uploads/ProPic/Punyajith_Walgampaya_21638.jpg")
        max_pics = 4
        tmp_num = (len(self.title) + len(self.first_name) + len(self.last_name) + len(self.speciality) + len(self.speciality_description)) % max_pics
        pic_index = tmp_num + 1
        pic_string = "http://sgp18.siteground.asia/~whistle4/images/docs/doc" + str(pic_index) + ".jpg"
        self.profile_pic = pic_string

        # TODO: use actual thumbnail, not randomly generated one:
        # using updated cropped picture from Imaad
        # self.thumbnail = json.get(kThumbnailPicture, "http://sgp18.siteground.asia/~whistle4/images/UpdatedPlaceholder.png")
        # self.thumbnail = json.get(kThumbnailPicture, "http://sgp18.siteground.asia/~whistle4/images/imaad-test.png")
        thumbnail_version = random.randint(1, 2)
        thumbnail_string = "http://sgp18.siteground.asia/~whistle4/images/thumbnails/doc" + str(pic_index) + "-thumb" + "-v" + str(thumbnail_version) + ".jpg"
        self.thumbnail = thumbnail_string

        # always add the speciality to the search tag
        self.search_tags = json.get(kSearchTag, "")
        if self.search_tags is None:
            self.search_tags = ""
        self.search_tags += self.speciality + ", "

        self.slmc_number = json.get(kSLMCNumber, "SLMC 12345")

        # TODO: Fix how we add Doctors, Wellness and Professionals Tag
        if self.speciality == "Oncologist":
            self.search_tags += "Doctors"
        elif self.speciality == "Nutritionist":
            self.search_tags += "Wellness"
        elif self.speciality == "Neonatologist":
            self.search_tags += "Professionals"

        # TODO: fix how price is calculated. Hardcoded to 1899 now
        # quote = json.get(kQuote, {})
        # self.price = "LKR " + str(quote.get(kValue, "1899.00"))
        self.price = "LKR 1899"

        # self.practice_locations = json.get(kPracticeLocations, ["oDoc Video Consultations, Worldwide", "Durdans Hospital, Colombo", "Asiri Central Hospital, Colombo"])
        self.practice_locations = ["oDoc Video Consultations, Worldwide", "Durdans Hospital, Colombo", "Asiri Central Hospital, Colombo"]

        self.hometown = json.get(kHometown, "Kurunegala")
        self.board_certification = json.get(kBoardCertification, "PGIM Sri Lanka")

        self.canSinhala = json.get(kCanSinhala, True)
        self.canTamil = json.get(kCanTamil, True)
        self.canEnglish = json.get(kCanEnglish, True)
        self.languages_spoken = ""
        if self.canEnglish:
            self.languages_spoken += "English "
        if self.canTamil:
            self.languages_spoken += "Tamil "
        if self.canSinhala:
            self.languages_spoken += "Sinhala "

        self.num_reviews = json.get(kNumReviews, 163)
        self.rating = json.get(kRating, 5)

        self.testimonials = list()

        # TODO: Adding one hardcoded testimonial
        self.testimonials.append({"name": "Jack Smith",
                                  "text": "Wow what a great doctor. Will use again!",
                                  "date": "Apr 1, 2018",
                                  "profile_pic": "http://caringestateliquidators.com/wp-content/uploads/2015/02/my-profile-pic-circular2.png"})

        response_testimonials = json.get(kTestimonials, list())
        for testimonial in response_testimonials:
            text = testimonial.get(kTestimonialText, "")
            # TODO: Adding hardcoded testimonial profile picture
            # profile_pic = testimonial.get(kTestimonialProfilePic, "http://sgp18.siteground.asia/~whistle4/images/TestimonialPlaceholder.png")
            profile_pic = "http://sgp18.siteground.asia/~whistle4/images/TestimonialPlaceholder.png"
            name = testimonial.get(kTestimonialName, "")
            date = testimonial.get(kTestimonialDate, "Jan 11, 2018")

            self.testimonials.append({"name": name,
                                      "text": text,
                                      "date": date,
                                      "profile_pic": profile_pic})

        self.rowTitles = json.get(kRowTitles, list())

        self.think_about_odoc = json.get(kThoughtsOnOdoc, "oDoc offers outcomes similar to face-to-face visits but with far lower costs for patients.")
        self.similar_docs = list()

    def to_dict(self, view_type):
        doctor_dict = {
            kDoctorID: self.doctor_id,
            kTitle: self.title,
            kFirstName: self.first_name,
            kLastName: self.last_name,
            kRating: self.rating,
            kPrice: self.price,
            kNumReviews: self.num_reviews
        }

        if view_type == "homescreen":
            doctor_dict.update({kWaittime: self.wait_time})
            doctor_dict.update({kThumbnailPicture: self.thumbnail})
            doctor_dict.update({kSearchTag: self.search_tags})
        elif view_type == "details":
            doctor_dict.update({kSLMCNumber: self.slmc_number})
            doctor_dict.update({kSpotText: self.spot_text})
            doctor_dict.update({kAwards: self.awards})
            doctor_dict.update({kSimilarDoctors: self.similar_docs})
            doctor_dict.update({kProfilePicture: self.profile_pic})
            doctor_dict.update({kSpeciality: self.speciality})
            doctor_dict.update({kSimilarDoctorsTitleString: "More " + self.speciality + "s"})
            doctor_dict.update({kSpecialityDescription: self.speciality_description})
            doctor_dict.update({kCityOfResidence: self.cityOfResidence})
            doctor_dict.update({kCountryOfResidence: self.countryOfResidence})
            doctor_dict.update({kStatement: self.statement})
            doctor_dict.update({kEducation: self.education})
            doctor_dict.update({kPracticeLocations: self.practice_locations})
            doctor_dict.update({kHometown: self.hometown})
            doctor_dict.update({kBoardCertification: self.board_certification})
            doctor_dict.update({kLanguagesSpoken: self.languages_spoken})
            doctor_dict.update({kTestimonials: self.testimonials})
            doctor_dict.update({kThoughtsOnOdoc: self.think_about_odoc})

        return doctor_dict
