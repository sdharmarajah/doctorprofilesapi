#!/usr/bin/env python
import view
import APIManager
from flask import Flask
from flask import request
from flask import make_response
from flask import jsonify
import json
import os

import settings


# Flask app should start in global layout
app = Flask(__name__)

# global: doctor_list for doctor profiles, row_titles for ordering
doctor_list = list()
row_titles = {}
explore_list = list()
odocstatus = list()
homescreen_list = {}


@app.route('/refresh', methods=['GET'])
def refresh():
    if request.method == 'GET':
        global doctor_list, row_titles, homescreen_list, explore_list, odocstatus
        doctor_list, row_titles, explore_list, odocstatus = APIManager.generate_data()
        homescreen_list = view.generate_homescreen(doctor_list, row_titles)
        resp = make_response("Refreshed.", 200)
        return resp


@app.route('/odocstatus', methods=['GET'])
def odocstatus():
    if request.method == 'GET':
        global odocstatus
        if len(odocstatus) == 0:
            resp = make_response("odocstatus list was null. Please make a GET request to /refresh", 200)
        else:
            resp = make_response(jsonify(odocstatus), 200)
    else:
        resp = make_response("Unknown request")
    return resp



@app.route('/clear', methods=['GET'])
def clear():
    if request.method == 'GET':
        global doctor_list, row_titles, homescreen_list, explore_list
        doctor_list = list()
        row_titles = {}
        homescreen_list = {}
        explore_list = list()
        resp = make_response("Cleared.", 200)
        return resp


@app.route('/explore', methods=['GET'])
def explore():
    if request.method == 'GET':
        global explore_list
        if len(explore_list) == 0:
            resp = make_response("Explore list was null. Please make a GET request to /refresh", 200)
        else:
            resp = make_response(jsonify(explore_list), 200)
    else:
        resp = make_response("Unknown request")
    return resp


@app.route('/doctors', methods=['GET', 'POST'])
def doctors():
    if request.method == 'GET':
        global homescreen_list
        if len(doctor_list) == 0:
            resp = make_response("Doctor list was null. Please make a GET request to /refresh", 200)
        else:
            resp = make_response(jsonify(homescreen_list), 200)
    elif request.method == 'POST':
        data = request.data
        data_dict = json.loads(data)
        doctor_id = data_dict.get('doctor_id')
        row_title = data_dict.get('row_title')
        resp = make_response(jsonify(view.get_doctor_details(doctor_list=doctor_list,
                                                             doctor_id=doctor_id,
                                                             row_title=row_title)))
    else:
        resp = make_response("Unknown request")
    return resp


@app.route('/rows', methods=['GET'])
def rows():
    if request.method == 'GET':
        global row_titles
        resp = make_response(jsonify(row_titles), 200)
        return resp


@app.route('/webapp', methods=['GET'])
def doctor_details_api():
    if request.method == 'GET':
        try:
            doctor_id = request.args.get("dr")
            if doctor_id is not None:
                doctor_id = doctor_id.strip()
                response = APIManager.get_doctor_details(doctor_id=doctor_id)
                resp = make_response(json.dumps(response), 200)
            else:
                message = "Could not process: 'dr' was null or not passed."
                resp = make_response(message, 200)
        except Exception as e:
            message = "Something went wrong: " + str(e)
            resp = make_response(message, 200)
    else:
        resp = make_response("Something other than GET...", 500)
    return resp

@app.route('/homescreen', methods=['GET'])
def get_homescreen_api():
    if request.method == 'GET':
        try:
            response = APIManager.get_homescreen_doctors()
            resp = make_response(json.dumps(response), 200)
        except Exception as e:
            message = "Something went wrong: " + str(e)
            resp = make_response(message, 200)
    else:
        resp = make_response("Something other than GET...", 500)
    return resp


if __name__ == '__main__':
    port = int(os.getenv('PORT', settings.flask_port))

    print("Starting app on port %d" % port)

    app.run(debug=True, port=port, host='0.0.0.0')
