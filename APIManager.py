import settings
import requests
import json
from Doctor import Doctor
from pprint import pprint
from Explore import Explore


def generate_explore():
    explore_list = list()
    explore_list.append(Explore(image_url="http://sgp18.siteground.asia/~whistle4/images/explore1.png",
                                display_text="Doctors",
                                search_text="Doctors").to_dict())
    explore_list.append(Explore(image_url="http://sgp18.siteground.asia/~whistle4/images/explore2.png",
                                display_text="Wellness",
                                search_text="Wellness").to_dict())
    explore_list.append(Explore(image_url="http://sgp18.siteground.asia/~whistle4/images/explore3.png",
                                display_text="Professionals",
                                search_text="Professionals").to_dict())
    return explore_list

def getODocStatus():
    # return oDoc Status
    payload = {"PatientId": 2}
    r = requests.post(settings.GetODocStatus, data=payload)
    print(r.status_code, r.reason)
    return json.loads(r.text)


def generate_data():
    # return get doctors in queue as dict, with doctor ID as key
    response = getODocStatus()
    doctor_json = response[settings.kDoctorsInQueue]

    # temporary list of doctors
    generated_doctor_list = list()
    for doc in doctor_json:
        generated_doctor_list.append(Doctor(doc))

    # extract the row titles
    generated_row_titles = {}

    temp_row_titles = {}
    for doctor in generated_doctor_list:
        for row in doctor.rowTitles:
            if row["sortOrder"] not in temp_row_titles:
                # first time we're seeing this sort order
                new_dict = {row["title"]: row["subtitle"]}
                temp_row_titles.update({row["sortOrder"]: new_dict})
            else:
                # sort order already existed.
                # check if the tuple existed
                temp_dict = temp_row_titles[row["sortOrder"]]
                temp_dict.update({row["title"]: row["subtitle"]})
                temp_row_titles[row["sortOrder"]] = temp_dict

    row_order = 0
    for k1, v1 in temp_row_titles.items():
        for k2, v2 in v1.items():
            # generated_row_titles.update({str(row_order): (k2, v2)})
            generated_row_titles.update({str(row_order): (k2, v2)})
            row_order += 1

    # pprint("Here are the row titles!")
    # pprint(generated_row_titles)

    # generate the similar doctors for these doctors


    # generate the explore tab
    explore_list = generate_explore()

    return generated_doctor_list, generated_row_titles, explore_list, doctor_json

    # # set the doctors in queue
    # doctors_in_queue = {}
    # for doctor_json in doctor_json:
    #     doctors_in_queue.update({
    #         doctor_json[settings.kDoctorID]: Doctor(doctor_json).to_dict()
    #     })
    #
    # # extract the row titles
    # row_titles = {}
    # for doctor_id, doctor in doctors_in_queue.items():
    #     print("doctor_id = " + str(doctor_id))
    #     for row in doctor.rowTitles:
    #         row_titles.update({row["sortOrder"]:(row["title"], row["subtitle"])})
    # return doctors_in_queue, row_titles

def get_doctor_details(doctor_id):
    # fetch doctor details
    endpoint = "https://dev.gen2.odoc.life/consultants/v1/primaryConsultants/dr_" + str(doctor_id)
    headers = {
        'Authorization': "Bearer A+mKDnt42OB2W+1Uny45iQxgT0YSLiOLTDnBJZ2zepY=",
        'Accept': "application/json",
        'Content-Type': "application/json"
    }
    r = requests.get(url=endpoint, headers=headers)
    print(r.status_code, r.reason)
    return json.loads(r.text)


def get_homescreen_doctors():
    # fetch homescreen doctors
    url = "https://dev.odoc.life/oDocApi/api/Patient/GetHomeScreenDoctors"

    payload = "{\n\t\"PatientId\": 2014\n}"
    headers = {
        'Content-Type': "application/json",
        'Authorization': "Bearer A+mKDnt42OB2W+1Uny45iQxgT0YSLiOLTDnBJZ2zepY=",
        'cache-control': "no-cache",
        'Postman-Token': "6380526f-e916-46bb-9376-e22bc09d2187"
        }

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)
    return json.loads(response.text)


if __name__ == "__main__":
    doctor_list, row_titles = generate_data()
    pprint(doctor_list)
