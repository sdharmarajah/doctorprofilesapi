import settings
import requests
import json
from Doctor import Doctor
from pprint import pprint


def getODocStatus():
    # return oDoc Status
    payload = {
        "PatientId": 2
    }
    r = requests.post(settings.GetODocStatus, data=payload)
    print(r.status_code, r.reason)
    return json.loads(r.text)


def getDoctorsInQueue():
    # return get doctors in queue as dict, with doctor ID as key
    POST_response = getODocStatus()
    doctorJSON = POST_response[settings.kDoctorsInQueue]
    pprint(doctorJSON)

    doctors_in_queue = {}
    for doctor_json in doctorJSON:
        doctors_in_queue.update({
            doctor_json[settings.kDoctorID]: Doctor(doctor_json).to_dict()
        })
    return doctors_in_queue


if __name__ == "__main__":
    doctors_in_queue = getDoctorsInQueue()
    pprint(doctors_in_queue)
